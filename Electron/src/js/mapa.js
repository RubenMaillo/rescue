const axios = require('axios');

var mymap = L.map('mapid').setView([40.4636688, -4.7492199], 6);
//https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox/streets-v11'
}).addTo(mymap);

var coords;
var radio = 25;
var zona, descr, nombre;

//redimensionar el radio de la zona en caso de cambiarlo
function redimensionar(r) {
    zona.setRadius((r.value * 1000));
    radio = r.value;
}

//Crear una zona de busqueda en el mapa
function onMapClick(e) {
    if(zona != undefined){
        mymap.removeLayer(zona);
    }

    zona = L.circle(e.latlng, radio * 1000, {
        color: '#1EB27D',
        fillColor: 'rgba(30, 178, 125, .5)',
        fillOpacity: 0.5,
        radius: radio
    }).addTo(mymap);

    //guardamos las coordenadas
    coords = [e.latlng.lat.toFixed(7).toString(), e.latlng.lng.toFixed(7).toString(), radio*1000];

    document.getElementById("coord").value = e.latlng.lat.toFixed(7).toString() + " / " + e.latlng.lng.toFixed(7).toString();
}

mymap.on('click', onMapClick);


//Añadir busqueda a la BBDD


async function addBusqueda() {
    zona = document.getElementById("zona").value;
    nombre = document.getElementById("nombre").value;
    descr = document.getElementById("descripcion").value;

    console.log(coords)
    var response = await axios.post("http://localhost:3000/alertas", {
        nombre: nombre,
        descripcion: descr,
        zona: zona,
        fechaCreacion: new Date(),
        coordenadas: coords,
        esActiva: true,
    });

    if(response.data.ok){
        alert("Alerta creada");
        window.close();
    }

    mandarNotificacion();
}

