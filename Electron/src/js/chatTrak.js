const socketChat = require('socket.io-client')('http://localhost:2222')

function pintarChat(mensajes){

    var dMensajes = document.getElementById("mensajes")

    mensajes.forEach(mensaje => {
        var msg = document.createElement('div')
        msg.id = mensaje._id;
        if(mensaje.id_usuario != "5e2e934ffd0dd625889e2f44")
            msg.className = 'msg';
        else
            msg.className = 'msgSelf';

        var fecha = document.createElement('p');
        fecha.innerHTML = mensaje.fecha.toString();
        fecha.className = "fecha";
        msg.appendChild(fecha);

        var mens = document.createElement('p');
        mens.innerHTML = mensaje.mensaje;
        mens.className = "mens";
        msg.appendChild(mens);

        dMensajes.appendChild(msg)
    });

    dMensajes.scrollTop = dMensajes.scrollHeight;
}

function enviarMensaje(){
    var mensaje = document.getElementById("mensaje").value
    data = {"id_alerta": idAlert,
            "id_usuario": "5e2e934ffd0dd625889e2f44",
            "fecha": new Date(),
            "mensaje": mensaje}
    socketChat.emit("mensajeChat", data)

    axios.post("http://localhost:3000/chat", {
                idAlerta: idAlert,
                idUsuario: "5e2e934ffd0dd625889e2f44",
                mensaje: mensaje,
                fecha: new Date(),
    })

    pintarMensaje(data)
    document.getElementById("mensaje").value = ""
}

socketChat.on("mensajeChat", function(data){
    pintarMensaje(data)
})

function pintarMensaje(mensaje){
    var dMensajes = document.getElementById("mensajes")

    var msg = document.createElement('div')
    msg.id = mensaje._id;
    if(mensaje.id_usuario != "5e2e934ffd0dd625889e2f44")
        msg.className = 'msg';
    else
        msg.className = 'msgSelf';

    var fecha = document.createElement('p');
    fecha.innerHTML = mensaje.fecha.toString();
    fecha.className = "fecha";
    msg.appendChild(fecha);

    var mens = document.createElement('p');
    mens.innerHTML = mensaje.mensaje;
    mens.className = "mens";
    msg.appendChild(mens);

    dMensajes.appendChild(msg)

    dMensajes.scrollTop = dMensajes.scrollHeight;
}

socketChat.on("traks", function(data){
    // //pintarTrak(data)

    // traks.then(t => {
    //     t.forEach(trak => {
        
            
    //             console.log("--> " + trak[0])
            
    //     })
    // })

    
    // console.log("··> " + data.id_alerta)
    
    //console.log(allTraks)
    // allTraks.forEach(a => {
    //     if(data.idUsuario == a._leaflet_id){
    //         console.log(a._latlngs)
    //         j = a._latlngs.push(data.coordenadas)
    //         //a.setLanLngs(j)
    //         mapa.removeLayer(a)
    //         console.log(a._latlngs)
    //         //a.redraw()
    //     }
    // })

    /*for(i in mapaAlert._layers) {
        if(mapaAlert._layers[i]._path != undefined) {
            try {
                mapaAlert.removeLayer(mapaAlert._layers[i]);
                mostrarAlerta()
            }
            catch(e) {
                console.log("problem with " + e + mapaAlert._layers[i]);
            }
        }
    }*/
   
})

function mandarNotificacion(){
    socketChat.emit("nuevaAlerta", "Se ha creado una alerta")
}