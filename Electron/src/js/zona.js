const {ipcRenderer} = require('electron')
const axios = require('axios');
var idAlerta;
var cords = [];
var zonas;

async function cargarZonas(){
    var response = await axios.get('http://localhost:3000/zonas');
    return response.data['json'];
    
}

async function selecZona(){
    cont = 1;
    var zonasSelect = document.getElementById("oAlertas");
    zonas = await cargarZonas();

    zonas.forEach(element => {
    
        var oZona = document.createElement('option')
        oZona.id = cont;
        oZona.value = element._id;
        
        oZona.innerHTML = element.zona;
        cont ++;

        zonasSelect.appendChild(oZona);
    });
}

function mostrarDatos() {
    var id = document.getElementById("oAlertas").value;

    zonas.forEach(element => {
        if(id == element._id){
            document.getElementById("nombre").value = element.zona;            
        }
        if(id == 0){
            document.getElementById("nombre").value = null;
        }
    });
}

async function modZona(){
    var id = document.getElementById("oAlertas").value;

    var response = await axios.post("http://localhost:3000/zonas/modZona", {
            id: id,
            nombre: document.getElementById("nombre").value,
        });

        if(response.data.ok){
            alert("Zona modificada");
            window.close();
        }
}

async function delZona(){
    var id = document.getElementById("oAlertas").value;

    if(id == 0){
        alert("Por favor, seleccione una zona");
    }
    else{
        var response = await axios.post("http://localhost:3000/zonas/delZona", {
            id: id,
        });

        if(response.data.ok){
            alert("Zona eliminada");
            window.close();
        }
    }    
}


function addZona(){
    var id = document.getElementById("oAlertas").value;

    if(id == 0)
        alert("Por favor, seleccione una alerta");
    else
        ipcRenderer.send('crearZona', id);
}

function verId(){
    ipcRenderer.on('id', (event, id) => {
        getAlerta(id);
    });
}

async function getAlerta(id){
    idAlerta = id;
    var response = await axios.post('http://localhost:3000/alertas/getAlerta', {
        id: id,
    });

    var element = response.data.alerta

    var cord = [parseFloat(element.coordenadas[0]), parseFloat(element.coordenadas[1])];

    L.circle(cord, {
        color: '#1EB27D',
        fillColor: 'rgba(30, 178, 125, .5)',
        fillOpacity: 0.5,
        radius: element.coordenadas[2]
    }).addTo(mymap);


    
    mymap.on('click', function(e){
        cords.push(e.latlng);
        pintarZona(cords);
    })
}

var polygon

function pintarZona(cords){
    if(polygon != null)
        polygon.remove();
    polygon = L.polygon(cords).addTo(mymap);
}

async function nuevaZona(){
    var zona = document.getElementById("zona").value;
    console.log(cords)

    if(zona == "")
        alert("Por favor, indique el nombre de la zona de busqueda.");
    else{
        console.log(cords)
        var response = await axios.post('http://localhost:3000/zonas', {
            idAlerta: idAlerta,
            zona: zona,
            coords: cords,
        });

        if(response.data.ok){
            alert("Zona creada con éxito");
            ipcRenderer.on('cerrar', (event, zona) => {
                console.log("cerrar");
            })
        }
    }
}
