const axios = require('axios');

// function cargarMapa(){
    var mymap = L.map('mapid').setView([40.4636688, 0], 6);
    //https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        id: 'mapbox/streets-v11'
    }).addTo(mymap);
// }

var alertasDiv = document.getElementById("alertas");
var cont = 1;
var alertas;

async function cargarAlertas(){
    var response = await axios.get('http://localhost:3000/alertas');
    return response.data['json'];
}

async function pintarAlertas() {
    cont = 1;
    alertas = await cargarAlertas();
    

    alertas.forEach(element => {
        
        var cord = [parseFloat(element.coordenadas[0]), parseFloat(element.coordenadas[1])];

        var dAlerta = document.createElement('div')
        dAlerta.id = element._id;
        dAlerta.className = 'alerta';
        dAlerta.onclick = mostrarAlerta;

        var title = document.createElement('p');
        title.innerHTML = element.nombre;
        title.className = "title";
        dAlerta.appendChild(title);

        var descr = document.createElement('p');
        descr.innerHTML = element.descripcion;
        descr.className = "descr";
        dAlerta.appendChild(descr);

        alertasDiv.appendChild(dAlerta);

        var area = L.circle(cord, {
            color: '#1EB27D',
            fillColor: 'rgba(30, 178, 125, .5)',
            fillOpacity: 0.5,
            radius: element.coordenadas[2]
        });

        area.bindPopup("<p><b>Nombre:</b> " + element.nombre + "</p><p><b>Descripción:</b> " + element.descripcion);
        area.addTo(mymap)
    });
}

async function selecAlerta(){
    cont = 1;
    var alertasSelect = document.getElementById("oAlertas");
    alertas = await cargarAlertas();

    alertas.forEach(element => {
    
        var oAlerta = document.createElement('option')
        oAlerta.id = cont;
        oAlerta.value = element._id;
        
        oAlerta.innerHTML = element.nombre;
        cont ++;

        alertasSelect.appendChild(oAlerta);
    });
}

function mostrarDatos(){
    console.log(alertas);
    var id = document.getElementById("oAlertas").value;

    alertas.forEach(element => {
        if(id == element._id){
            document.getElementById("nombre").value = element.nombre;
            document.getElementById("descripcion").value = element.descripcion;
            document.getElementById("zona").value = element.zona;
            document.getElementById("fecha").value = element.fechaCreacion;
            document.getElementById("coord").value = element.coordenadas[0] + ", " + element.coordenadas[1];
            document.getElementById("radio").value = element.coordenadas[2];
            if(element.esActiva)
                document.getElementById("activa").value = "true";
            else
                document.getElementById("activa").value = "false";
            
        }
        if(id == 0){
            document.getElementById("nombre").value = null;
            document.getElementById("descripcion").value = null;
            document.getElementById("zona").value = null;
            document.getElementById("coord").value = null;
            document.getElementById("radio").value = null;
        }
    });
}

async function modAlerta(){
    var id = document.getElementById("oAlertas").value;

    if(id == 0){
        alert("Por favor, seleccione una alerta");
    }
    else{
        var nombre = document.getElementById("nombre").value;
        var descr = document.getElementById("descripcion").value;
        var zona = document.getElementById("zona").value;
        var fecha = document.getElementById("fecha").value;
        var coords = document.getElementById("coord").value.split(", ");
        var radio = document.getElementById("radio").value;
        var activa = document.getElementById("activa").value;

        coords.push(radio);

        var response = await axios.post("http://localhost:3000/alertas/modAlerta", {
            id: id,
            nombre: nombre,
            descripcion: descr,
            zona: zona,
            fechaCreacion: fecha,
            coordenadas: coords,
            esActiva: activa,
        });

        if(response.data.ok){
            alert("Alerta modificada");
            window.close();
        }
    }    
}

async function delAlerta(){
    var id = document.getElementById("oAlertas").value;

    if(id == 0){
        alert("Por favor, seleccione una alerta");
    }
    else{
        var response = await axios.post("http://localhost:3000/alertas/delAlerta", {
            id: id,
        });

        if(response.data.ok){
            alert("Alerta eliminada");
            window.close();
        }
    }    
}

function toggleAlerta(clear){
    document.getElementById("infoAlerta").classList.toggle("desplegado");
    document.getElementById("iconoChat").classList.toggle("despl");
    if(clear)
        location.reload();
}

async function cargarInfo(idAlertaa){
    var res = await axios.post('http://localhost:3000/alertas/infoAdicional', {
        id: idAlertaa
    });

    return res.data.json
}

var idAlert = null
var traks = null
var allTraks = []
var mapaAlert = null
var infoA = null

function mostrarAlerta() {
    
    toggleAlerta(false);

    // cargarChat();
    alertas.forEach(element => {
        if(this.id == element._id || this.idAlert == element._id){
            idAlert = element._id
            cargarChat(element._id);
            var infoAdicional = cargarInfo(element._id)
            

            //Mostrar info de la alerta

            var dInfo = document.getElementById("info")

            var tnombre = document.createElement('h3');
            tnombre.innerHTML = "Nombre";
            dInfo.appendChild(tnombre);

            var inombre = document.createElement('p');
            inombre.innerHTML = element.nombre;
            dInfo.appendChild(inombre);

            var tdescr = document.createElement('h3');
            tdescr.innerHTML = "Descripcion";
            dInfo.appendChild(tdescr);

            var idescr = document.createElement('p');
            idescr.innerHTML = element.descripcion;
            dInfo.appendChild(idescr);

            infoAdicional.then(ia => {
                var tusu = document.createElement('h3');
                tusu.innerHTML = "Nº usuarios";
                dInfo.appendChild(tusu);

                var iusu = document.createElement('p');
                iusu.innerHTML = ia[0].numUsu;
                dInfo.appendChild(iusu);

                var tduracion = document.createElement('h3');
                tduracion.innerHTML = "Duración de la busqueda";
                dInfo.appendChild(tduracion);

                var iduracion = document.createElement('p');
                iduracion.innerHTML = ia[0].diferencia.toFixed(2) + " horas" ;
                dInfo.appendChild(iduracion);
            })

            

                        
            
            
            
            //Añadir al mapa los datos de la alerta
            var cord = [parseFloat(element.coordenadas[0]), parseFloat(element.coordenadas[1])];

            mapaAlert = L.map('mapa').setView(cord, 10);

            
            L.circle(cord, {
                color: '#1EB27D',
                fillColor: 'rgba(30, 178, 125, .5)',
                fillOpacity: 0.5,
                radius: element.coordenadas[2]
            }).addTo(mapaAlert);

            var zonas = cargarZonas(element._id)
            zonas.then(z => {
                z.forEach(zona => {
                    L.polygon(zona.coordenadas).addTo(mapaAlert);
                })
            })

            traks = getTrak()
            traks.then(t => {
                t.forEach(trak => {
                    
                    //console.log(element._id + " / " + trak.id_alerta)
                    if(element._id == trak.id_alerta){
                        //console.log(trak)
                        var tr = L.polyline(trak.coordenadas, {
                            color: '#ff0000'
                        })
                        //console.log(tr)
                        tr._leaflet_id = trak.id_usuario
                        tr.addTo(mapaAlert)
                        allTraks.push(tr)
                    }
                })
            })
            
        }
    });

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        id: 'mapbox/streets-v11'
    }).addTo(mapaAlert);
}

async function cargarZonas(id){
    var response = await axios.post("http://localhost:3000/zonas/getZonasAlerta", {
        id: id
    })

    var zonas = response.data.json
    return zonas
}

function mostrarChat() {
    document.getElementById("chat").classList.toggle("chatAbierto");
    document.getElementById("iconoChat").classList.toggle("despl");
}

async function cargarChat(id){
    var response = await axios.post("http://localhost:3000/chat/getCurrentChat", {
        idAlerta: id
    })

    pintarChat(response.data.chat)
}

function cerrarChat(){
    document.getElementById("chat").classList.toggle("chatAbierto");
    document.getElementById("iconoChat").classList.toggle("despl");
}

async function getTrak(){
    var response = await axios.get("http://localhost:3000/trak");
    return response.data.trak
}

async function numUsuarios(id){
    var response = await axios.post("http://localhost:3000/usuario/numberOfUsersInAlert", {
        idAlerta: id
    })

    console.log(response.data)
}
