const { BrowserWindow } = require("electron");

/**
 *
 * browserWindow, la panatalla padre
 * width, anchura pantalla modal
 * height altura pantalla modal
 * url, url del código a cargar en el proceso renderer
 */
function createFormModal(browserWindow, width, height, url) {
    let winForm = new BrowserWindow({
        width: width,
        height: height,
        frame: false,
        parent: browserWindow,
        modal: true
    })
    winForm.loadURL(url)
}

// browserWindow es la pantalla sobre la que se quiere crear el menú
let mainMenu = browserWindow => {
    let templateMenu = [
        {
            label: "Usuarios",
            submenu: [
                {
                    label: "Crear usuario",
                    accelerator: "CommandOrControl+C",
                    click() {
                        let url = `file://${__dirname}/../html/usuarios/crearUsuario.html`;
                        createFormModal(browserWindow, 400, 400, url);
                    }
                },
                {
                    label: "Modificar usuario",
                    accelerator: "CommandOrControl+U",
                    click() {
                        let url = `file://${__dirname}/../html/usuarios/modificarUsuario.html`;
                        createFormModal(browserWindow, 420, 500, url);
                    }
                },
                {
                    label: "Ver usuario",
                    accelerator: "CommandOrControl+R",
                    click() {
                        let url = `file://${__dirname}/../html/usuarios/verUsuario.html`;
                        createFormModal(browserWindow, 420, 490, url);
                    }
                },
                {
                    label: "Borrar usuario",
                    accelerator: "CommandOrControl+D",
                    click() {
                        let url = `file://${__dirname}/../html/usuarios/borrarUsuario.html`;
                        createFormModal(browserWindow, 400, 470, url);
                    }
                },
                {
                    label: "Asignar usuario a grupo",
                    accelerator: "CommandOrControl+D",
                    click() {
                        let url = `file://${__dirname}/../html/usuarios/asignarGrupo.html`;
                        createFormModal(browserWindow, 400, 280, url);
                    }
                },
                {
                    label: "Borrar usuario de grupo",
                    accelerator: "CommandOrControl+D",
                    click() {
                        let url = `file://${__dirname}/../html/usuarios/borrarUsuGrupo.html`;
                        createFormModal(browserWindow, 400, 220, url);
                    }
                },
            ]
        },
        {
            label: "Búsquedas",
            submenu: [
                {
                    label: "Crear búsqueda",
                    accelerator: "CommandOrControl+B",
                    click() {
                        let url = `file://${__dirname}/../html/busquedas/crearBusqueda.html`;
                        createFormModal(browserWindow, 1200, 750, url);
                    }
                },
                {
                    label: "Modificar búsqueda",
                    accelerator: "CommandOrControl+G",
                    click() {
                        let url = `file://${__dirname}/../html/busquedas/modificarBusqueda.html`;
                        createFormModal(browserWindow, 450, 650, url);
                    }
                },
                {
                    label: "Borrar búsqueda",
                    accelerator: "CommandOrControl+M",
                    click() {
                        let url = `file://${__dirname}/../html/busquedas/eliminarBusqueda.html`;
                        createFormModal(browserWindow, 400, 250, url);
                    }
                }
            ]
        },
        {
            label: "Zonas",
            submenu: [
                {
                    label: "Crear zona de busqueda",
                    accelerator: "CommandOrControl+P",
                    click() {
                        let url = `file://${__dirname}/../html/zonas/crearZona.html`;
                        createFormModal(browserWindow, 420, 270, url);
                    }
                },
                {
                    label: "Modificar zona de busqueda",
                    accelerator: "CommandOrControl+O",
                    click() {
                        let url = `file://${__dirname}/../html/zonas/modificarZona.html`;
                        createFormModal(browserWindow, 420, 370, url);
                    }
                },
                {
                    label: "Eliminar zona de busqueda",
                    accelerator: "CommandOrControl+I",
                    click() {
                        let url = `file://${__dirname}/../html/zonas/borrarZona.html`;
                        createFormModal(browserWindow, 420, 490, url);
                    }
                },
                {
                    label: "Ver usuarios de zonas",
                    accelerator: "CommandOrControl+J",
                    click() {
                        let url = `file://${__dirname}/../html/zonas/verUsuarios.html`;
                        createFormModal(browserWindow, 400, 550, url);
                    }
                },
            ]
        },
        {
            label: "View",
            submenu: [
                { role: "reload" },
                { role: "forcereload" },
                { role: "toggledevtools" },
                { type: "separator" },
                { role: "resetzoom" },
                { role: "zoomin" },
                { role: "zoomout" },
                { type: "separator" },
                { role: "togglefullscreen" }
            ]
        }
    ];

    return templateMenu;
};

module.exports.mainMenu = mainMenu;
