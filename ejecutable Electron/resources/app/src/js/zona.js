const {ipcRenderer} = require('electron')
var idAlerta;
var cords = [];

function addZona(){
    var id = document.getElementById("oAlertas").value;

    if(id == 0)
        alert("Por favor, seleccione una alerta");
    else
        ipcRenderer.send('crearZona', id);
}

function verId(){
    ipcRenderer.on('id', (event, id) => {
        getAlerta(id);
    });
}

async function getAlerta(id){
    idAlerta = id;
    var response = await axios.post('http://localhost:3000/alertas/getAlerta', {
        id: id,
    });

    var element = response.data.alerta

    var cord = [parseFloat(element.coordenadas[0]), parseFloat(element.coordenadas[1])];

    L.circle(cord, {
        color: '#1EB27D',
        fillColor: 'rgba(30, 178, 125, .5)',
        fillOpacity: 0.5,
        radius: element.coordenadas[2]
    }).addTo(mymap);


    
    mymap.on('click', function(e){
        cords.push(e.latlng);
        pintarZona(cords);
    })
}

var polygon

function pintarZona(cords){
    if(polygon != null)
        polygon.remove();
    polygon = L.polygon(cords).addTo(mymap);
}

async function nuevaZona(){
    var zona = document.getElementById("zona").value;
    console.log(cords)

    if(zona == "")
        alert("Por favor, indique el nombre de la zona de busqueda.");
    else{
        console.log(cords)
        var response = await axios.post('http://localhost:3000/zonas', {
            idAlerta: idAlerta,
            zona: zona,
            coords: cords,
        });

        if(response.data.ok){
            alert("Zona creada con éxito");
            ipcRenderer.on('cerrar', (event, zona) => {
                console.log("cerrar");
            })
        }
    }
}
