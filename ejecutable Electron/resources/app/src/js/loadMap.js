'use strict'

var mymap = L.map('mapid').setView([40.4636688, -4.7492199], 6);
//https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox/streets-v11'
}).addTo(mymap);