//import liraries
import React, { Component } from 'react';
import { 
    View, 
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Alert
} from 'react-native';

import MapView, {
    Marker,
    AnimatedRegion,
    Polyline,
    Circle,
    Polygon,
    PROVIDER_GOOGLE
} from "react-native-maps";

import Socket from 'Rescue/src/conexion'
import SocketIo  from 'Rescue/src/conexion/SocketIo';
import { observer } from 'mobx-react'
import { toJS } from 'mobx'
import chat from 'Rescue/src/assets/comment-regular.png'
import waypoint from 'Rescue/src/assets/search-location-solid.png'
import pin from 'Rescue/src/assets/map-pin-solid.png'
import Chat from 'Rescue/src/components/Chat'
import Geolocation from '@react-native-community/geolocation';

@observer
// create a component
class Busqueda extends Component {

    static navigationOptions = {
        title: "RESCUE",
        headerTintColor: "#e4e4e4",
        headerStyle: {
            backgroundColor: 'black'
        },
    }

    constructor(props) {
        super(props)
        
        this.state = {
            chatVisible: false,
            latitude: parseFloat(Socket.rescate.coordenadas[0]),
            longitude: parseFloat(Socket.rescate.coordenadas[1]),
            latitudeDelta: 1.5,
            longitudeDelta: 1.5
        }

       SocketIo.sendTrak(this)
    }

    renderItem(item, index) {
        
        var coords = item.coordenadas
        var coordenadas = []
        coords.forEach(element => {
            var cord = {"latitude": element.lat,"longitude": element.lng}
            coordenadas.push(cord)
        });
 
        return (
            <Polygon key={item._id}
                coordinates={coordenadas}
                strokeWidth={2}
                strokeColor="#428df5"
                fillColor="rgba(66, 141, 245, .3)"
            />
        );
    }

    renderMarkers(item, index) {
        var coordenadas = {"latitude": item.coordenadas[0].lat,"longitude": item.coordenadas[0].lng}
        
        return (
            <Marker.Animated
                key={item._id}  
                coordinate = {coordenadas}
            />
        );
    }

    renderTraks(item, index) {
        var correcto; 
        if(item.id_alerta == Socket.rescate._id){
            var coords = item.coordenadas
            var coordenadas = []
            coords.forEach(element => {
                var cord = {"latitude": element.lat,"longitude": element.lng}
                coordenadas.push(cord)
            });
            correcto = true
        }else{
            correcto = false
        }

        if(correcto){
            var color = "#00ff6e"
            if(item.id_usuario == Socket.user._id)
                color = "#ff7300"
            return (
                <Polyline
                    key={item._id}
                    coordinates = {coordenadas}
                    strokeColor= {color}
		            strokeWidth={3}
                    lineCap="round"
                />
            );
        }
    }

    renderWayPoint(item, index) {
        if(item.id_alerta == Socket.rescate._id){
            var coordenadas = {"latitude": item.coordenadas[0].lat,"longitude": item.coordenadas[0].lng}
            
            return (
                <Marker
                    key={item._id}  
                    coordinate = {coordenadas}
                    pinColor = {'teal'}
                    ref={marker => {
                        this.marker1 = marker;
                    }}
                    onCalloutPress={() => {this.marker1.hideCallout();}}
                        >
                    <MapView.Callout>
                        <View>
                            <Text>{item.mensaje}</Text>
                        </View>
                    </MapView.Callout>
                    {/* <Image style={styles.marker} source={pin}/> */}
                </Marker>
            );
        }
    }

    

    getMapRegion = () => ({
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        latitudeDelta: this.state.latitudeDelta,
        longitudeDelta: this.state.longitudeDelta
    });

    onRegionChangeComplete = region => {
        this.setState({latitude: region.latitude,
            longitude: region.longitude,
            latitudeDelta: region.latitudeDelta,
            longitudeDelta: region.longitudeDelta})
    }

    componentDidMount(){
        Socket.getChat()
    }

    componentWillUnmount() {
        SocketIo.clearTrakInterval();
    }

    toggleChat = () => {
        this.setState({ chatVisible: !this.state.chatVisible });
    }

    sendWayPoint = () => {
        Alert.alert(
            //title
            "WayPoint",
            //body
            '¿Elija un WayPoint?',
            [
                {text: 'Cancelar', onPress: () => console.log('Cancelado'), style: 'cancel'},
                {text: 'Pista', onPress: () => SocketIo.sendWayPoint("Pista")},
                {text: 'Encontrado', onPress: () => SocketIo.sendWayPoint("Encontrado")},
            ],
            { cancelable: false }
            //clicking out side of alert will not cancel
        );
    }

    render() {
        return (
            <View style={styles.map}>
                <MapView
                    style={styles.map}
                    provider={PROVIDER_GOOGLE}
                    zoomEnabled={true}
                    region={this.getMapRegion()}
                    onRegionChangeComplete={this.onRegionChangeComplete.bind(this)}
                >
                    <Circle 
                        center={{
                            latitude: parseFloat(Socket.rescate.coordenadas[0]),
                            longitude: parseFloat(Socket.rescate.coordenadas[1]),
                        }}
                        radius={parseInt(Socket.rescate.coordenadas[2])}
                        strokeWidth={2}
                        strokeColor="#1EB27D"
                        fillColor="rgba(30, 178, 125, .4)"
                    />

                    {toJS(Socket.zonas).map((item, index) => this.renderItem(item, index))}
                    {toJS(Socket.usuariosAlerta).map((item, index) => this.renderMarkers(item, index))}
                    {toJS(SocketIo.traks).map((item, index) => this.renderTraks(item, index))}
                    {toJS(SocketIo.waypoint).map((item, index) => this.renderWayPoint(item, index))}

                </MapView>
                

                <TouchableOpacity style={styles.chat} onPress={this.toggleChat}> 
                    <Image style={styles.icon} source={chat}/>
                </TouchableOpacity>

                <TouchableOpacity style={styles.waypoint} onPress={this.sendWayPoint}> 
                    <Image style={styles.icon} source={waypoint}/>
                </TouchableOpacity>

                <Chat visible = {this.state.chatVisible} onCloseModal = {this.toggleChat}/>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        height: "100%",
        justifyContent: "flex-end",
        alignItems: "center"
      },
      map: {
        flex: 1
      },
      buttonContainer: {
        width: "50%",
        flexDirection: "row",
        marginVertical: 20,
        backgroundColor: "transparent"
      },
      botones: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-around",
      },
      chat: {
        position: "absolute",
        bottom: 15,
        right: 15,
        width: 50,
        height: 50,
        borderRadius: 50,
        backgroundColor: "#1EB27D",
        justifyContent: "center",
        alignItems: "center",
      },
      waypoint: {
        position: "absolute",
        bottom: 75,
        right: 15,
        width: 50,
        height: 50,
        borderRadius: 50,
        backgroundColor: "#ffffff",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#1EB27D",
        borderWidth: 1,
      },
      icon: {
        width: 30,
        height: 30,
      },
      marker: {
            width: 23,
            height: 40
      }
});

//make this component available to the app
export default Busqueda;
