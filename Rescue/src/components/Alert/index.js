//import liraries
import React, {Component} from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import Home from 'Rescue/src/screens/home';
import {aUsuario} from 'Rescue/src/screens/signin'
import Socket from 'Rescue/src/conexion'
import SocketIo from 'Rescue/src/conexion/SocketIo'
import { observer } from 'mobx-react'

@observer
class MyAlerts extends Component {

    constructor(props) {
        super(props);
    }

    renderItem(item, index) {
        var alertas = Socket.user.alertas
        console.log(alertas.length)
        
        for(var i=0; i < alertas.length; i++){
            if(alertas[i] == item._id){
                return (
                    <TouchableOpacity key={item._id} style={styles.item} onPress={() => this.irAlerta(item)}>
                        <Text style={styles.title}>{item.nombre}</Text>
                        <Text style={styles.descripcion}>{item.descripcion}</Text>
                    </TouchableOpacity>
                );
            }
        }
    }

    irAlerta(item){
        Socket.getTrak();
        Socket.getUsersInAlert(item._id)
        var zona = Socket.getZonas(item._id)
        var alerta = Socket.getAlerta(item._id)
        Socket.getWayPoint()
        
        var that = this
        setTimeout(function(){
            if(zona && alerta)
                that.props.nav.navigate("Busqueda", {alerta: item})
            else
                alert("No se han podido cargar los datos")
        }, 500)
    }

    render() {

        return (
            <React.Fragment>
                <SafeAreaView style={styles.container}>
                    <ScrollView  style={styles.scroll}>
                        {Socket.alertas.map((item, index) => this.renderItem(item, index))}
                    </ScrollView>
                </SafeAreaView>
            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    scroll: {
        width: "95%",
    },
    item: {
        width: "100%",
        backgroundColor: '#f4f4f4',
        padding: 20,
        marginVertical: 8,
        borderRadius: 10,
    },
    title: {
        fontSize: 20,
        color: "#1EB27D",
    },
    descripcion: {
        fontSize: 15,
    },
    
  });

//make this component available to the app
export default MyAlerts;
