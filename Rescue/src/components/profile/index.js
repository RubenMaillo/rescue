import React, { Component } from 'react';
import { 
  View, 
  Text, 
  StyleSheet, 
  TextInput,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {aUsuario} from 'Rescue/src/screens/signin'
import Socket from 'Rescue/src/conexion'

class Profile extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: {},
    };
  }

  render() {
    return (
      <React.Fragment>
        <View style={styles.container}>
            <Text style={styles.text}>DNI</Text>
            <TextInput style={styles.input} placeholder="DNI" value={Socket.user.dni} placeholderTextColor="#E5E5E5" />

            <Text style={styles.text}>Name</Text>
            <TextInput style={styles.input} placeholder="Name" value={Socket.user.nombre} placeholderTextColor="#E5E5E5" />

            <Text style={styles.text}>Last name</Text>
            <TextInput style={styles.input} placeholder="Last name" value={Socket.user.apellidos} placeholderTextColor="#E5E5E5" />

            <Text style={styles.text}>Last name</Text>
            <TextInput style={styles.input} placeholder="Phone number" value={Socket.user.telefono} placeholderTextColor="#E5E5E5" />
        </View>
      </React.Fragment>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
      flex: 1,
      width: "100%",
      justifyContent: 'center',
      alignItems: 'center',
  },
  cont: {
      width: 40,
      height: 40,
      marginRight: 20,
      justifyContent: "center",
      alignItems: "center",
  },
  icon: {
      width: 22,
      height: 25,
  },
  text: {
    fontSize: 15,
    color: "#1EB27D",
  },
  input: {
    width: "70%",
    height: 40,
    textAlign: "center",
    backgroundColor: "rgba(0, 0, 0, .8)",
    margin: 10,
    letterSpacing: 3,
    color: "#E5E5E5",
    fontSize: 15,
  },
});

export default Profile;
