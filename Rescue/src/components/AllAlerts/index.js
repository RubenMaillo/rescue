//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, Alert, Picker, Modal, Button } from 'react-native';
import Socket from 'Rescue/src/conexion'
import { observer } from 'mobx-react'
import {aUsuario} from 'Rescue/src/screens/signin'

@observer
class AllAlerts extends Component {

    constructor(props) {
        super(props);

        this.state = {
            
            modalVisible: false,
        }
        
    }

    renderItem(item, index) {
        return (
            <TouchableOpacity key={item._id} style={styles.item} onPress={() => this.confirmarAlerta(item)}>
                <Text style={styles.title}>{item.nombre}</Text>
                <Text style={styles.descripcion}>{item.descripcion}</Text>
            </TouchableOpacity>
            
        );
    }

    confirmarAlerta(item){
        
        Alert.alert(
            //title
            item.nombre,
            //body
            'Va a unirse a este rescate,\n ¿Desea continuar?',
            [
                {text: 'Cancelar', onPress: () => console.log('No Pressed'), style: 'cancel'},
                {text: 'Aceptar', onPress: () => this.irAlerta(item)},
            ],
            { cancelable: false }
            //clicking out side of alert will not cancel
        );
    }

    irAlerta(item){
        Socket.getAllAlerts()
        Socket.getAlertasUsuario(Socket.user._id)
        Socket.addUserAlerta(item._id, Socket.user._id)
    }

    render() {
        return (
            <React.Fragment>
                <SafeAreaView style={styles.container}>
                    <ScrollView  style={styles.scroll}>
                        {Socket.alertas.map((item, index) => this.renderItem(item, index))}
                    </ScrollView>
                </SafeAreaView>
            </React.Fragment>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    scroll: {
        width: "95%",
    },
    item: {
        width: "100%",
        backgroundColor: '#f4f4f4',
        padding: 20,
        marginVertical: 8,
        borderRadius: 10,
    },
    title: {
        fontSize: 20,
        color: "#1EB27D",
    },
    descripcion: {
        fontSize: 15,
    },
    
  });

//make this component available to the app
export default AllAlerts;
