
import {ToastAndroid} from 'react-native'
import socketIO from 'socket.io-client';
import { observable, action, computed, toJS } from 'mobx';
import SocketApi from 'Rescue/src/conexion'
import { observer } from 'mobx-react'
import Geolocation from '@react-native-community/geolocation';
import { Component } from 'react';
import UpdateTrak from 'Rescue/src/components/UpdateTraks'

const URLServer = "http://192.168.0.14:2222";
//const URLServer = "http://10.18.49.115:2222";
//const URLServer = "http://10.10.17.140:2222";


// create a component
class Socket{
   socket = socketIO.connect(URLServer, { 'forceNew': true });

   @observable _chat = []

   @action setChat(valor){
      this._chat = valor
   }

   @computed get chat(){
      return this._chat
   }

   @observable _traks = []

   @action setTraks(valor){
      this._traks = valor
   }
   
   @computed get traks(){
      return this._traks
   }

   @observable _waypoint = []

   @action setWayPoint(valor){
      this._waypoint = valor
   }

   @computed get waypoint(){
      return this._waypoint
   }
   
   constructor(){
      this.inicializarHandlers(this);
   }

   inicializarHandlers(t){
      //socket
      this.socket.on("connect", function(){
         ToastAndroid.show("Conectado al servidor en el puerto 2222", ToastAndroid.SHORT);
      });

      this.socket.on("mensajeChat", function(data){
         t.chat.push(data);
      });

      this.socket.on("traks", function(data){
            toJS(t.traks).forEach((element, index) => {
               if(element.id_usuario == SocketApi.user._id && element.id_alerta == SocketApi.rescate._id){
                  element.coordenadas.push(element.coordenadas)

                  var ua = SocketApi.usuariosAlerta
                  ua.map(item => {
                     if(item._id == SocketApi.user._id)
                        item.coordenadas = element.coordenadas
                  }) 
               }
            })

            
      });

      this.socket.on("nuevaAlerta", function(data){
         console.log("notification: " + data)
      })
   }

   mandar(mensaje){
      SocketApi.sendMensaje(mensaje.mensaje);

      //Envio del mensaje por socket al servidor
      this.socket.emit("mensajeChat", mensaje);
   }

   sendWayPoint(data){
      
      var mensaje
      if(data == "Encontrado")
         mensaje = "Encontrado"
      else
         mensaje = "He encontrado una pista"
      
      var newMensaje = {"id_alerta": SocketApi.rescate._id,
                        "id_usuario": SocketApi.user._id,
                        "fecha": new Date(),
                        "mensaje": mensaje}
      this.chat.push(newMensaje)

      //Envio del mensaje por socket al servidor
      this.socket.emit("mensajeChat", newMensaje);


      SocketApi.sendWayPoint(data);
      SocketApi.sendMensaje(mensaje);
   }

   timer = null

   async sendTrak(t){
      
      var that = this
      var sum1 = 0.00
      var sum2 = 0.00
      this.timer = setInterval(await function(){
         t.watchId = Geolocation.getCurrentPosition(position => {
            const newCoordinate = {
                "lat": position.coords.latitude + sum1,
                "lng": position.coords.longitude + sum2
            };

            sum1 += (Math.random() * (0.004 - (-0.004)) + (-0.004))
            sum2 += (Math.random() * (0.004 - (-0.004)) + (-0.004))

            var coordenadas = {"idAlerta": SocketApi.rescate._id, "idUsuario": SocketApi.user._id, "coordenadas": newCoordinate}
            SocketApi.sendTrak(coordenadas)

            SocketApi.user.coordenadas=coordenadas.coordenadas

            var ua = SocketApi.usuariosAlerta
            ua.map(item => {
               if(item._id == SocketApi.user._id)
                  item.coordenadas = [coordenadas.coordenadas]
            })

            SocketApi.getTrak();
            that.socket.emit("traks", coordenadas);

            toJS(that.traks).forEach(element => {
               
               if(element.id_usuario == SocketApi.user._id && element.id_alerta == SocketApi.rescate._id){
                  element.coordenadas.push(newCoordinate)
               }
            })
         })
      }, 3000)
   }

   clearTrakInterval(){
      clearInterval(this.timer)
   }
}

let socket = new Socket()
export default socket;
