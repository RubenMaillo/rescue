//import liraries
import SocketIo  from 'Rescue/src/conexion/SocketIo';
import { DeviceEventEmitter, ToastAndroid } from 'react-native'
import { observable, action, computed } from 'mobx'
import Geolocation from '@react-native-community/geolocation';

const url = "http://192.168.0.14:3000"
//const url = "http://10.18.49.115:3000"

class Socket {

    @observable _user = false

    @action setUser(valor){
        this._user = valor
    }
    
    @computed get user(){
        return this._user
    }
    
    login(user, pass){
        //http://10.18.49.115:3000/login
        //http://192.168.0.14:3000/login
    
        var login = fetch(url + '/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                user: user,
                pass: pass,
            }),
        });
    
        return login;
    }

    @observable _modal = false

    @action setModal(valor){
        this._modal = valor
    }
    
    @computed get modal(){
        return this._modal
    }

    
    @observable _alertas = []

    @action setAlertas(valor){
        this._alertas = valor
    }
    
    @computed get alertas(){
        return this._alertas
    }

    @observable _rescate = []

    @action setRescate(valor){
        this._rescate = valor
    }
    
    @computed get rescate(){
        return this._rescate
    }

    @observable _zonas = []

    @action setZonas(valor){
        this._zonas = valor
    }
    
    @computed get zonas(){
        return this._zonas
    }
    
    getAllAlerts(){
        fetch(url + '/alertas')
        .then((response) => response.json())
        .then((responseJson) => {

           this.setAlertas(responseJson.json)
           //console.log(this.alertas)
        })
        .catch((error) => {
            console.error(error);
        });
    }

    addUserAlerta(idAlerta, idUsuario){
        Geolocation.getCurrentPosition(position => {
            const newCoordinate = {
                "lat": position.coords.latitude,
                "lng": position.coords.longitude
            }

            var zonas = fetch(url + '/zonas/addUserAlerta', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: idAlerta,
                    idUsuario: idUsuario,
                    coordenadas: newCoordinate,
                }),
            })
        });
    }

    async getAlerta(id){
        var correcto;

        await fetch(url + '/alertas/getAlerta', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: id
            }),
        }).then((response) =>{
            response.json().then(resc => {
                if(resc.ok){
                    this.setRescate(resc.alerta)
                    return true;
                }
                else{
                    return false
                }
            })
        }).catch((error) => {
            console.error(error);
            return false
        });

    }

    async getZonas(id){
        var correcto;
        await fetch(url + '/zonas/getZonasAlerta', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: id
            }),
        }).then((response) => {
            response.json().then(zona => {
                if(zona.ok){
                    this.setZonas(zona.json)
                    return true;
                }
                else{
                    return false
                }
            })        
        }).catch((error) => {
            console.error(error);
            return false
        });
    }

    async getAlertasUsuario(id){
        await fetch(url + '/usuario/getCurrentUser', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: id
            }),
        }).then((response) => {
            response.json().then(usuario => {
                this.setUser(usuario.usuario)
            })        
        }).catch((error) => {
            console.error(error);
        });
    }

    @observable _usuariosAlerta = []

    @action setusuariosAlerta(valor){
        this._usuariosAlerta = valor
    }
    
    @computed get usuariosAlerta(){
        return this._usuariosAlerta
    }

    async getUsersInAlert(id){
        await fetch(url + '/usuario/getUsersInAlert', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: id
            }),
        }).then((response) => {
            response.json().then(usuario => {
                this.setusuariosAlerta(usuario.usuarios)
            })        
        }).catch((error) => {
            console.error(error);
        });
    }

    getChat(){
        fetch(url + "/chat/getCurrentChat", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idAlerta: this.rescate._id,
            })
        }).then((response) => {
            response.json().then(chat => {
                SocketIo.setChat(chat.chat)
            })        
        }).catch((error) => {
            console.error(error);
        });
    }

    sendMensaje(data){
        fetch(url + "/chat", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idAlerta: this.rescate._id,
                idUsuario: this.user._id,
                mensaje: data,
                fecha: new Date(),
            })
        })
    }

    getTrak(){
        fetch(url + "/trak").then((response) => {
            response.json().then(trak => {
                SocketIo.setTraks(trak.trak)
            })        
        }).catch((error) => {
            console.error(error);
        });
    }

    sendTrak(coordenadas){
        fetch(url + "/trak", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idAlerta: coordenadas.idAlerta,
                idUsuario: coordenadas.idUsuario,
                coordenadas: coordenadas.coordenadas,
            })
        })
    }

    getWayPoint(){
        fetch(url + "/waypoint").then((response) => {
            response.json().then(waypoint => {
                SocketIo.setWayPoint(waypoint.waypoint)
            })        
        }).catch((error) => {
            console.error(error);
        });
    }

    sendWayPoint(data) {

        Geolocation.getCurrentPosition(position => {
            const newCoordinate = {
                "lat": position.coords.latitude,
                "lng": position.coords.longitude
            }

            fetch(url + "/waypoint", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    idAlerta: this.rescate._id,
                    mensaje: data,
                    coordenadas: newCoordinate,
                })
            })
        });

        
    }
}

let socket = new Socket()
export default socket;
