var express = require('express');
var app = express();
var CWayPoint = require('../controllers/CWayPoint.js');

app.get('/', CWayPoint.getAllWayPoint); // Visualiza TODOS los WayPoint
app.post('/', CWayPoint.insertWayPoint); // Añade un WayPoint

module.exports = app;

