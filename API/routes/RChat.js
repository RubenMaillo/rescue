var express = require('express');
var app = express();
var CChat = require('../controllers/CChat.js');

app.get('/', CChat.getAllChat); // Visualiza TODOS los mensajes
app.post('/', CChat.insertMessage); // Añade un mensaje
app.post('/getCurrentChat', CChat.getCurrentChat); // Añade un mensaje

module.exports = app;

