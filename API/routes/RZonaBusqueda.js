var express = require('express');
var app = express();
var CZonasBusqueda = require('../controllers/CZonaBusqueda.js')

app.get('/', CZonasBusqueda.getAllZonas); // Recoger todas las zona
app.post('/', CZonasBusqueda.addZona); // Añadir una zona
app.post('/modZona', CZonasBusqueda.modZona); // modificar una zona
app.post('/delZona', CZonasBusqueda.delZona); // modificar una zona
app.post('/getZonasAlerta', CZonasBusqueda.getZonasAlerta); // Recoger todas las zonas de una zona
app.post('/addUserAlerta', CZonasBusqueda.addUserAlerta); // Añadir un usuario a una zona

module.exports = app;
