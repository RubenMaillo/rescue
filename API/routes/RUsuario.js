var express = require('express');
var app = express();
var CUsuario = require('../controllers/CUsuario.js');

app.get('/', CUsuario.getAllUsers); // Visualiza TODOS los usuarios
app.post('/getCurrentUser', CUsuario.getUser); // actual usuario
app.post('/', CUsuario.insertUser); // Añade un usuario
app.post('/getUsersInAlert', CUsuario.getUsersInAlert); // Devuelve los usuarios de una alerta
app.post('/numberOfUsersInAlert', CUsuario.numberOfUsersInAlert); // Añade un usuario   

module.exports = app;
