var express = require('express');
var app = express();
var CTrak = require('../controllers/CTrak.js');

app.get('/', CTrak.getAllTrak); // Visualiza TODOS los traks
app.post('/', CTrak.insertTrak); // Añade un trak

module.exports = app;

