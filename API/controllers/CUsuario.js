var Usuario = require('../models/MUsuario');
var bcrypt = require('bcryptjs');

function getAllUsers(req, res, next) {
    Usuario.find({}).exec((err, json) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            json
        });
    });
}

function getUser(req, res, next){

    const { id } = req.body;

    Usuario.findById(id, (err, usuario) => {

        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            usuario
        });
    });
};


function insertUser(req, res, next){
    var { dni, nombre, apellidos, telefono, password } = req.body;

    var usuario = new Usuario({
        dni: dni,
        nombre: nombre,
        apellidos: apellidos,
        telefono: telefono,
        password: bcrypt.hashSync(password, 10),
        admin: false,
        alertas: [],
        id_alerta: [],
        coordenadas: []
    });

    usuario.save(( err, usuarioGuardado ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el usuario',
                errors: err
            });
        }
    });
}

function addZonaUser(req, res, next){
    console.log(req.body)
    var { id, idUsuario, coordenadas } = req.body;
    console.log(coordenadas)
    Usuario.findById(idUsuario, (err, usuario) => {

        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        if(usuario){
            usuario.id_alerta.push(id);
            usuario.alertas.push(id);
            usuario.coordenadas = [coordenadas]
            usuario.save();
        }

    });
}

function getUsersInAlert(req, res, next){
    var { id } = req.body;
    Usuario.find({alertas: id}).exec((err, usuarios) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            usuarios
        });
    });

}

function numberOfUsersInAlert(req, res, next){
    var { idAlerta } = req.body;

    console.log("Entraaaaaaaaaaaaaaa")

    Usuario.aggregate([
        {
            $match: {"id_alerta": id}
        },{
            $count: "numUsuarios"
        }
        
    ]).exec((err, usuarios) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            usuarios
        });
    });
}

function modCoord(req, res, next){
    var { id, idUsuario, coordenadas } = req.body;

    console.log(coordenadas)

    Usuario.findById(idUsuario, (err, usuario) => {

        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        if(usuario){
            usuario.coordenadas = [coordenadas]
            usuario.save();
        }

    });
}

module.exports = {
    getAllUsers,
    getUser,
    insertUser,
    addZonaUser,
    getUsersInAlert,
    numberOfUsersInAlert,
    modCoord
};

