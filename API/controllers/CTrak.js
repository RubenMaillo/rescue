var Trak = require('../models/MTrak');
var bcrypt = require('bcryptjs');
var Cusuario = require('./CUsuario')

function getAllTrak(req, res, next) {
    Trak.find({}).exec((err, trak) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            trak
        });
    });
}

function insertTrak(req, res, next){
    var { idAlerta, idUsuario, coordenadas } = req.body;

    Trak.find({"id_usuario": idUsuario, "id_alerta": idAlerta}).exec((err, trak) => {
        if (err){
            //console.log("No hay usuario")
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        if(trak.length > 0){
            trak[0].coordenadas.push(coordenadas)
            Cusuario.modCoord(req, res, next)

            trak[0].save(( err, trakSave ) => {
                if (err){
                    return res.status(400).json({
                        ok: false,
                        mensaje: 'Error al actualizar el trak',
                        errors: err
                    });
                }
                res.status(200).json({
                    ok: true,
                    trak: trakSave
                });
            });
        }
        else{
            var trak = new Trak({
                id_alerta: idAlerta,
                id_usuario: idUsuario,
                coordenadas: coordenadas,
            });
        
            trak.save(( err, mensajeSave ) => {
                if (err){
                    return res.status(400).json({
                        ok: false,
                        mensaje: 'Error al crear el usuario',
                        errors: err
                    });
                }
            });
        }
    });
}

module.exports = {
    getAllTrak,
    insertTrak,
};

