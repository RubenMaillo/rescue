var bcrypt = require('bcryptjs');
var Alerta = require('../models/MAlerta');
var mongoose = require('mongoose')

function getAllAlertas(req, res, next) {
    Alerta.find({}).sort({fechaCreacion: -1}).exec((err, json) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            json
        });
    });
}

function addAlerta(req, res, next){
    var { nombre, descripcion, zona, fechaCreacion, coordenadas, esActiva } = req.body;

    alerta = new Alerta({
        nombre: nombre,
        descripcion: descripcion,
        zona: zona,
        fechaCreacion: fechaCreacion,
        coordenadas: coordenadas,
        esActiva: esActiva,
        usuarios: []
    });

    alerta.save( ( err, alertaGuardada ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear la alerta',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            alerta: alertaGuardada
        });
    });
}

function modAlerta(req, res, next){
    var { id, nombre, descripcion, zona, fechaCreacion, coordenadas, esActiva } = req.body;

    Alerta.findById(id, (err, alerta) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al modificar la búsqueda',
                errors: err
            });
        }

        if (!alerta){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una alerta con ese id',
            });
        }

        alerta.nombre = nombre;
        alerta.descripcion = descripcion;
        alerta.zona = zona;
        alerta.fechaCreacion = alerta.fechaCreacion;
        alerta.coordenadas = coordenadas;
        alerta.esActiva = esActiva;
        alerta.usuarios = alerta.usuarios;

        alerta.save((err, alertaGuardada) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al guardar los nuevos datos',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                paciente: alertaGuardada
            });
        })

    })
}

function delAlerta(req, res, next){
    var { id } = req.body;

    Alerta.findByIdAndRemove( id, ( err, alertaDel ) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar la alerta',
                errors: err
            });
        }

        if (!alertaDel){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una alerta con ese ID',
            });
        }

        res.status(200).json({
            ok: true,
            consulta: alertaDel
        });
    })
    
}

function getAlerta(req, res, next){
    var { id } = req.body;

    Alerta.findById( id, ( err, alerta ) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar la alerta',
                errors: err
            });
        }

        if (!alerta){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una alerta con ese ID',
            });
        }

        res.status(200).json({
            ok: true,
            alerta
        });
    })
    
}

function getInfoAdicional(req, res, next) {
    var { id } = req.body;
    var ida = mongoose.Types.ObjectId(id);
    console.log(ida)

    Alerta.aggregate([
        {
            $match: { _id: ida }
        }, {
            $project: {
                numUsu: { $size: "$usuarios" },
                usuarios: "$usuarios",
                diferencia: {$divide: [{ $subtract: [new Date(), "$fechaCreacion"]}, 60*1000*60 ]}
            }
        }
    ]).exec((err, json) => {

        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al seleccionar alerta',
                errors: err
            });
        }

        console.log(json)
        res.status(200).json({
            ok: true,
            json
        });
    });

}

function addUserAlerta(req, res, next){

    var { id, idUsuario, coordenadas } = req.body;

    Alerta.findById(id, (err, alerta) => {

        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        if(alerta){
            alerta.usuarios.push(idUsuario);
            alerta.save();
        }
    });
}

module.exports = {
    getAllAlertas,
    addAlerta,
    modAlerta,
    delAlerta,
    getAlerta,
    getInfoAdicional,
    addUserAlerta
};

