var bcrypt = require('bcryptjs');
var Zona = require('../models/MZonaBusqueda');
var mongoose = require('mongoose')
var Cusuario = require('./CUsuario')
var Calerta = require('./CAlerta')


function getAllZonas(req, res, next) {
    Zona.find({}).exec((err, json) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            json
        });
    });
}

function addZona(req, res, next){
    var { idAlerta, zona, coords } = req.body;

    console.log(idAlerta + " / " + zona + " / " + JSON.stringify(coords));

    zona = new Zona({
        id_alerta: idAlerta,
        id_usuario: [],
        zona: zona,
        coordenadas: coords,
    });

    zona.save( ( err, zonaGuardada ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear la alerta',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            alerta: zonaGuardada
        });
    });
}

function modZona(req, res, next){
    var { id, nombre } = req.body;

    Zona.findById(id, (err, zona) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al modificar la zona',
                errors: err
            });
        }

        if (!zona){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una zona con ese id',
            });
        }

        zona.zona = nombre

        zona.save((err, zonaGuardada) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el usuario',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                paciente: zonaGuardada
            });
        })

    })
}


function delZona(req, res, next){
    var { id } = req.body;

    Zona.findByIdAndRemove( id, ( err, zonaDel ) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar la alerta',
                errors: err
            });
        }

        if (!zonaDel){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una zona para con ese ID',
            });
        }

        res.status(200).json({
            ok: true
        });
    })
    
}

function getZonasAlerta(req, res, next) {
    var { id } = req.body;
    
    Zona.find({"id_alerta": id}).exec((err, json) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        //console.log(json)
        res.status(200).json({
            ok: true,
            json
        });
    });
}

function addUserAlerta(req, res, next) {
    var { id, idUsuario, coordenadas } = req.body;

    Zona.find({"id_alerta": id}).sort({id_usuario: 1}).limit(1).exec((err, zona) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }

        if(zona.length > 0){
            var existe = false
            console.log("Entra")
            console.log(zona[0].id_usuario.length)
            zona[0].id_usuario.map(item => {
                if(item == idUsuario)
                    existe = true
            })

            if(!existe){
                zona[0].id_usuario.push(idUsuario),
                Cusuario.addZonaUser(req, res, next)
                Calerta.addUserAlerta(req, res, next)

                zona[0].save(( err, zonaSave ) => {
                    if (err){
                        return res.status(400).json({
                            ok: false,
                            mensaje: 'Error al actualizar la zona',
                            errors: err
                        });
                    }
                    res.status(200).json({
                        ok: true,
                        usuario: zonaSave
                    });
                });
            }
        }
        else{
            res.status(200).json({
                json: "vacio"
            });

        }
    });
}

module.exports = {
    getAllZonas,
    addZona,
    modZona,
    delZona,
    getZonasAlerta,
    addUserAlerta,
};