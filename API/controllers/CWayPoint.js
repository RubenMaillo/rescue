var WayPoint = require('../models/MWayPoint');
var bcrypt = require('bcryptjs');

function getAllWayPoint(req, res, next) {
    WayPoint.find({}).exec((err, waypoint) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            waypoint
        });
    });
}

function insertWayPoint(req, res, next){
    var { idAlerta, mensaje, coordenadas } = req.body;

    console.log(idAlerta + " / " + mensaje + " / " + coordenadas)

    var waypoint = new WayPoint({
        id_alerta: idAlerta,
        mensaje: mensaje,
        coordenadas: [coordenadas]
    });

    waypoint.save(( err, waypointGuardado ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el usuario',
                errors: err
            });
        }
    });
    
}

module.exports = {
    getAllWayPoint,
    insertWayPoint
};

