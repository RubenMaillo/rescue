var Chat = require('../models/MChat');
var bcrypt = require('bcryptjs');

function getAllChat(req, res, next) {
    Chat.find({}).exec((err, chat) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            chat
        });
    });
}

function insertMessage(req, res, next){
    var { idAlerta, idUsuario, mensaje, fecha } = req.body;

    //console.log(idAlerta + "/" + idUsuario + "/" + mensaje + "/" + fecha)
    var chat = new Chat({
        id_alerta: idAlerta,
        id_usuario: idUsuario,
        mensaje: mensaje,
        fecha: fecha,
    });

    chat.save(( err, mensajeSave ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el usuario',
                errors: err
            });
        }
    });
}

function getCurrentChat(req, res, next){
    var { idAlerta } = req.body;

    Chat.find({"id_alerta": idAlerta}).sort({id_usuario: -1}).exec((err, chat) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        
        res.status(200).json({
            ok: true,
            chat
        });
    });
}



module.exports = {
    getAllChat,
    insertMessage,
    getCurrentChat
};

