var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var TrakSchema = new Schema({
    id_alerta: { type: Schema.Types.ObjectId, ref: 'MAlerta'},
    id_usuario: { type: Schema.Types.ObjectId, ref: 'MUsuario'},
    mensaje: { type: String, required: [true, 'El mensaje es obligatorio'] },
    fecha: { type: Date },
});

module.exports = mongoose.model('chat', TrakSchema);