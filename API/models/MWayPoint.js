var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var WayPointSchema = new Schema({
    id_alerta: { type: Schema.Types.ObjectId, ref: 'MAlerta'},
    mensaje: { type:String },
    coordenadas: { type: [JSON], required: [true, 'Las coordenadas son obligatorias'] },
});

module.exports = mongoose.model('waypoint', WayPointSchema);